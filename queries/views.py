# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.template.response import TemplateResponse
from django.http import JsonResponse
from django.db.models import Q
from django.contrib.auth.models import User 
from django.contrib.auth.decorators import login_required

@login_required
def user_query(request, *args, **kwargs):
  try:

    list_result, result, total = [], {}, 0

    if request.method == 'POST' and request.is_ajax():
      query_set = Q()

      if 'name' in request.POST:
        name = request.POST.get('name')
        query_set = query_set.__and__(Q(username__startswith = name))

      if 'email' in request.POST:
        email = request.POST.get('email')
        query_set = query_set.__and__(Q(email__icontains = email))

      if 'data_max' and 'data_min'  in request.POST:
        
        max_time = request.POST.get('data_max')
        min_time = request.POST.get('data_min')
        #print "data_max.server ==> " + str(max_time)
        #print "data_min.server ==> " + str (min_time)
 
        query_set = query_set.__and__(Q(date_joined__range=(min_time, max_time)))

      #LLenado de la tabla
      query = User.objects.filter(query_set)
      total = query.count()
      start = int(request.POST.get('iDisplayStart'))
      length = int(request.POST.get('iDisplayLength'))
      query = query[start:start+length]
      list_result = [user_list_datos(i) for i in query]
      result.update({'aaData': list_result, 'iTotalRecords': total, 'iTotalDisplayRecords': total,})
      return JsonResponse(result)

    elif request.method == 'GET':
      template = 'queries/tablas_user.html'
      return TemplateResponse(request, template)
  except Exception as e:
    print 'Exception in user_query => {}'.format(str(e))

def user_list_datos(datos):
  try:
    return [datos.username, datos.first_name, datos.last_name, datos.email, datos.date_joined]
  except Exception as e:
    print 'Exception in user_list_datos => {}'.format(str(e))
