$(document).ready(function() { 

	var my_token = csrftoken;
	var url_page = window.location.pathname;

	var tables = 	$('#table_user').DataTable( {
		'bServerSide': true,
		"searching": false,
		sAjaxSource: url_page,
		'fnServerData': function(sSource, aoData, fnCallback){
			aoData.push({
				'name': 'csrfmiddlewaretoken',
				'value': my_token
			});

			//filtrado

			if ($('#name').val() != ''){
				aoData.push({
					'name': 'name',
					'value': $('#name').val()
				});
			}
			
			if ($('#email').val() != ''){
				aoData.push({
					'name': 'email',
					'value': $('#email').val()
				});
			}

			if ($('#data_min').val() != '' && $('#data_max').val() != ''){
				//console.log('data_min == >'+ $('#data_min').val());
				//console.log('data_max == >' + $('#data_max').val());
				aoData.push({
					'name': 'data_max',
					'value': $('#data_max').val()
				});
				aoData.push({
					'name': 'data_min',
					'value': $('#data_min').val()
				});
			}

			$.ajax({
				'dataType': 'json',
				'type': 'POST',
				'url': sSource,
				'data': aoData,	
				'success': function(json){
					fnCallback(json);
				}
			});
		},
	} );

	$("#name").keyup(function(e) {
	  e.preventDefault();
	  tables.ajax.reload()
	});
  
  $("#email").keyup(function(e) {
	  e.preventDefault();
	  tables.ajax.reload()
	});
	
  $("#datetimepicker1").on("change.datetimepicker", function (e) {
  	$('#data_min').val(e.date.format());
  	//console.log('data_min.change == >'+ $('#data_min').val());
  	e.preventDefault();
  	tables.ajax.reload()
  });

  $("#datetimepicker2").on("change.datetimepicker", function (e) {
  	$('#data_max').val(e.date.format());
		//console.log('data_max.change == >' + $('#data_max').val());
    e.preventDefault();
	  tables.ajax.reload()
  });
});