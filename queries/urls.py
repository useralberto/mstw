# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
  url(r'^registered_user/', views.user_query, name='registered_user'),
]
