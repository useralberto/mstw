# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def index(request, *args, **kwargs):
 return render(request, 'core/index.html')

@login_required
def about(request, *args, **kwargs):
	return render(request, 'core/about.html')

@login_required
def user_registry(request, *args, **kwargs):
	return render(request, 'core/user_tables.html')