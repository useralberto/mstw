# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect

# Registro y confirmacion por email.
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.urls import reverse_lazy

# form y token
from .forms import SignUpForm
from .tokens import account_activation_token

def signup(request, *args, **kwargs):
	try:
		if request.method == 'POST':
			form = SignUpForm(request.POST)
			if form.is_valid():
				user = form.save(commit = False)
				user.is_active = False
				user.save()

				current_site = get_current_site(request)
				subject = 'Activa tu cuenta en Mi sitio.pythonanywhere.com'
				message = render_to_string('registration/account_activation_email.html', {
					'user': user,
					'domain': current_site.domain,
					'uid': urlsafe_base64_encode(force_bytes(user.pk)),
					'token': account_activation_token.make_token(user),
				})
				to_email = form.cleaned_data.get('email')
				user.email_user(subject, message)
				return render(request, 'registration/account_activation_sent.html')
		else:
			form = SignUpForm()
		return render(request, 'registration/signup.html', {'form': form})
	except Exception as e:
		print 'Exception in signup => {}'.format(str(e))

def account_activation_sent(request, *args, **kwargs):
	try:
		return render(request, 'registration/account_activation_sent.html')
	except Exception as e:
		print 'Exception in account_activation_sent => {}'.format(str(e))

def activate(request, uidb64, token, *args, **kwargs):
  try:
    uid = force_text(urlsafe_base64_decode(uidb64))
    user = User.objects.get(pk=uid)
  except (TypeError, ValueError, OverflowError, User.DoesNotExist):
    user = None

  if user is not None and account_activation_token.check_token(user, token):
    user.is_active = True
    user.profile.email_confirmed = True
    user.save()
    #login(request, user)
    return redirect('login')
  else:
    return render(request, 'registration/account_activation_invalid.html')
