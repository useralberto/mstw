# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignUpForm(UserCreationForm):

	first_name = forms.CharField(max_length = 30, required = False, help_text = 'Optional.')
	last_name = forms.CharField(max_length = 30, required = False, help_text = 'Optional.')
	email = forms.EmailField(max_length = 254, help_text = 'Requiered. Inform a valid email addres.')
	birt_date = forms.DateField(help_text = 'Requiered. Format: DD-MM-YYYY')
	password1 = forms.CharField(max_length=32, widget=forms.PasswordInput, help_text='Ingresa la contraseña')
	
	class Meta:
		model = User 
		fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)

	def clean_email(self):
		email = self.cleaned_data.get("email")
		if User.objects.filter(email = email).exists():
			raise forms.ValidationError("Lo sentimos, ese correo ya está registrado.")
		return email