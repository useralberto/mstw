# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
  url(r'^login/$', auth_views.login, {'template_name': 'registration/login.html'}, name='login'),
  url(r'^logout/$', auth_views.logout, {'next_page': 'login'}, name='logout'),
  url(r'^signup/$', views.signup, name='signup'),
  url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name = 'activate'),
  #reset password
  url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
  url(r'^password_reset/done$', auth_views.password_reset_done, name='password_reset_done'),
  url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
  url(r'^reset/done$', auth_views.password_reset_complete, name='password_reset_complete'),
]
