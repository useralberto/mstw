# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
#La creacion de una modelo Perfile en la base de datos que contredra una 
# * Relacion con el modelo use uno a uno
#	*	El campo de confirmacion por emial.
#	*	La fecha de nacimento
#	* Entre otros campos como se muestra.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default = False)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)

# Funcion para actualizar y manejar la creacion de los bjectos en la tabla Profiles.
#
@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()